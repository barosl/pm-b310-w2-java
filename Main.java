package com.barosl.pm;

import com.barosl.pm.PowerManager;
import java.io.IOException;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		PowerManager pm;

		try {
			pm = new PowerManager("192.168.0.28", 5000);
		} catch (IOException e) {
			System.err.println("Cannot connect to the power manager");
			System.exit(1);
			return;
		}

		Scanner sc = new Scanner(System.in);

		while (true) {
			int choice;
			System.out.println("(1) Retrieve power info (2) Reset statistics (3) Cut power (4) Exit");
			try { choice = Integer.parseInt(sc.nextLine()); }
			catch (NumberFormatException e) { continue; }

			if (choice == 1) {
				PowerManager.Info info;

				try { info = pm.getInfo(); }
				catch (IOException e) {
					System.err.println("Error while receiving information");
					System.exit(2);
					return;
				}

				System.out.println("Volts: " + info.volts);
				System.out.println("Amperes: " + info.amperes);
				System.out.println("Watts: " + info.watts);
				System.out.println("Watt-hours: " + info.wattHours);
				System.out.println("Charge: " + info.charge + " won");
				System.out.println("----");

			} else if (choice == 2) {
				try { pm.reset(); }
				catch (IOException e) {
					System.err.println("Unable to reset the statistics");
					continue;
				}
				System.out.println("Reset the statistics.");

			} else if (choice == 3) {
				try { pm.cut(); }
				catch (IOException e) {
					System.err.println("Unable to cut the power");
					continue;
				}
				System.out.println("Cut the power.");

			} else if (choice == 4) {
				break;
			}
		}

		pm.close();
	}
}
