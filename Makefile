.PHONY: all r run doc

all:
	rm -rf build
	mkdir build
	javac Main.java PowerManager.java -d build

r: run

run:
	java -cp build com.barosl.pm.Main

doc:
	rm -rf doc
	javadoc PowerManager.java -d doc
