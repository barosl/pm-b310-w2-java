package com.barosl.pm;

import java.net.Socket;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Manages a connection to the power manager, and retrieve power information from it.
 */
public class PowerManager {
	private static final byte[] PKT_HANDSHAKE = {
		(byte)0xfa, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x51,
		0x13, 0x19, 0x10, (byte)0xfb, 0x4a,
		0x0d
	};

	private static final byte[] PKT_HEADER = {
		(byte)0xfa, 0x31, 0x41, 0x43
	};

	private static final byte[] PKT_FOOTER = {
		0x0d
	};

	private static final byte[] PKT_RESPONSE = {
		0x31, 0x31, 0x45, 0x40
	};

	private static final byte[] PKT_REQUEST = {
		(byte)0xfa, 0x31, 0x41, 0x43,
		0x31, 0x31, 0x45, 0x74,
		0x50, 0x75, 0x73, 0x68, (byte)0xfb, 0x3d,
		0x0d,
	};

	private static final byte[] PKT_RESET = {
		(byte)0xfa, 0x31, 0x41, 0x43,
		0x31, 0x31, 0x45, 0x57,
		(byte)0xc1, 0x00, 0x00, (byte)0xfb, (byte)0xe1,
		0x0d,
	};

	private static final byte[] PKT_CUT = {
		(byte)0xfa, 0x31, 0x41, 0x43,
		0x31, 0x31, 0x45, 0x57,
		(byte)0xa4, 0x00, 0x00, 0x00, (byte)0xfb, (byte)0x84,
		0x0d,
	};

/*
	private static final byte[] PKT_RELAY = {
		(byte)0xfa, 0x31, 0x41, 0x43,
		0x31, 0x31, 0x45, 0x57,
		(byte)0x81, 0x00, 0x00, (byte)0xfb, (byte)0xa1,
		0x0d,
	};
*/

	/**
	 * Represents power information
	 */
	public class Info {
		public float volts;
		public float amperes;
		public float watts;
		public float wattHours;
		public float charge;
	}

	private Socket sock;
	private InputStream in;
	private OutputStream out;

	/**
	 * Creates a connection to the power manager
	 * @param host Hostname of the power manager
	 * @param port Port number of the power manager
	 * @throws IOException Network failures
	 */
	public PowerManager(String host, int port) throws IOException {
		sock = new Socket(host, port);
		in = sock.getInputStream();
		out = sock.getOutputStream();

		out.write(PKT_HANDSHAKE);
	}

	private byte[] receivePacket() throws IOException {
		byte[] buf = new byte[1024];
		int buf_pos = 0;

		while (true) {
			int b = in.read();

			if (b == -1) return null;
			if (buf_pos == buf.length) throw new IOException();

			buf[buf_pos++] = (byte)b;

			if (buf_pos >= PKT_HEADER.length && Arrays.equals(Arrays.copyOfRange(buf, buf_pos - PKT_HEADER.length, buf_pos), PKT_HEADER)) {
				byte[] res = Arrays.copyOfRange(buf, 0, buf_pos - PKT_HEADER.length);
				if (res.length == 0) {
					buf_pos = 0;
					continue;
				}

				if (!Arrays.equals(Arrays.copyOfRange(res, res.length - PKT_FOOTER.length, res.length), PKT_FOOTER)) {
					throw new IOException();
				}

				return Arrays.copyOfRange(res, 0, res.length - PKT_FOOTER.length);
			}
		}
	}

	/**
	 * Retrieves power information from power manager
	 * @throws IOException Network failures
	 * @return Power information
	 */
	public Info getInfo() throws IOException {
		byte[] buf;

//		out.write(PKT_HANDSHAKE);
//		out.write(PKT_REQUEST);
//		out.write(PKT_REQUEST);

		while (true) {
			out.write(PKT_REQUEST);
//			out.write(PKT_REQUEST);
			buf = receivePacket();

			if (Arrays.equals(Arrays.copyOfRange(buf, 0, PKT_RESPONSE.length), PKT_RESPONSE)) {
//				ByteArrayInputStream bin = new ByteArrayInputStream(buf);
//				DataInputStream din = new DataInputStream(bin);
//				System.out.println(din.readFloat());

				ByteBuffer bb = ByteBuffer.wrap(buf);
				bb.order(ByteOrder.LITTLE_ENDIAN);

				Info res = new Info();
				bb.getFloat();
				res.volts = bb.getFloat();
				res.amperes = bb.getFloat();
				bb.getFloat();
				bb.getFloat();
				res.watts = bb.getFloat();
				res.wattHours = bb.getFloat();
				res.charge = bb.getFloat();

				return res;
			}
		}

//		return null;
	}

	/**
	 * Closes the connection
	 */
	public void close() {
		try { if (in != null) in.close(); }
		catch (IOException e) { }

		try { if (out != null) out.close(); }
		catch (IOException e) { }

		try { if (sock != null) sock.close(); }
		catch (IOException e) { }

		sock = null;
		in = null;
		out = null;
	}

	/**
	 * Reset the statistics
	 */
	public void reset() throws IOException {
		out.write(PKT_RESET);
		receivePacket();
	}

	/**
	 * Cut the power
	 */
	public void cut() throws IOException {
		out.write(PKT_CUT);
		receivePacket();
	}
}
